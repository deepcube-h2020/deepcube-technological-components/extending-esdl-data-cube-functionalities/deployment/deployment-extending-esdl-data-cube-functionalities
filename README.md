# Deployment  Extending ESDL data cube functionalities

Deploy Extending ESDL data cube functionalities on the DeepCube Platform

## Getting started

1. `source vault.fish`
2. Create secrets with `ansible-vault create secret_vars/all.yml  --vault-password-file "./.vault_password"`
3. `ansible-playbook -i hosts/all.yml deploy.yml --vault-password-file "./.vault_password"`